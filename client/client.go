package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {
	conn, _ := net.Dial("tcp", "localhost:8080")
	stdin := bufio.NewReader(os.Stdin)
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	for true {

		fmt.Print(">  ")
		text, _ := stdin.ReadString('\n')
		writer.Write([]byte(text))
		writer.Flush()

		buf := make([]byte, 256)
		nbytes, err := reader.Read(buf)
		if err != nil {
			break
		}
		str := string(buf[:nbytes])
		fmt.Println("Server reply: ", str)

		if str == "Bye" {
			break
		}
	}

	conn.Close()
}
