package main

import (
	"fmt"
	"bufio"
	"net"
)

func handleClient(pConn *net.Conn) {
	conn := *pConn
	reader := bufio.NewReader(conn)
	writer := bufio.NewWriter(conn)

	for true {
		buf := make([]byte, 256)
		nbytes, _ := reader.Read(buf)
		clientText := string(buf[:nbytes-2])
		
		fmt.Println(clientText)
		fmt.Println(nbytes)

		if clientText == "quit" {
			writer.Write([]byte("Bye"))
			writer.Flush()
			break
		} else {
			writer.Write([]byte("You said: " + clientText))
			writer.Flush()
		}
	}

	conn.Close()
}

func main() {
	ln, _ := net.Listen("tcp", "localhost:8080")

	for true {
		conn, _ := ln.Accept()
		go handleClient(&conn)
	}

	ln.Close()
}
